function find(elements, cb){

    let result
    for(let index in elements){
        if(cb(elements[index])){
            result = elements[index]
            break
        }
        
    }
    return result

}

module.exports = find