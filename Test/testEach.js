let each = require("../each.js")

const items = [1, 2, 3, 4, 5, 5]

let result = each(items, (number,index,elements)=>{
    return `${number} is the element of index ${index}`
})

console.log(result)
