function reduce(elements, cb, startingValue){
    
    let index = 0
    if (startingValue == undefined){
        startingValue = elements[0]
        index++
    }
    for(;index < elements.length; index++){
        startingValue =cb(startingValue,elements[index])
    }

    return startingValue

}



module.exports = reduce