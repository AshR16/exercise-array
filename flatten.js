function flatten(elements){

    let result=[]
    for(let index in elements){
        if(Array.isArray(elements[index])){
            result.push(...flatten(elements[index]))
        }else{
            result.push(elements[index])
        }
    }
    return result

}

module.exports = flatten 