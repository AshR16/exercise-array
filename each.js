function each(elements, cb){
    
    let result = []
    for(let index in elements){
          result.push(cb(elements[index],index,elements))
    }
    return result
 
}

module.exports = each 